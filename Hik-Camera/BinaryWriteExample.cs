using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

namespace HikCamera;

public class BinaryWriteExample
{
    public void Reader()
    {
        int port = int.Parse("7200");

        TcpListener server = new TcpListener(IPAddress.Any, port);
        server.Start();
        
        while (true)
        {
            TcpClient client = server.AcceptTcpClient();
            NetworkStream stream = client.GetStream();

            MemoryStream memoryStream = new MemoryStream();
            byte[] buffer = new byte[50024];
            int bytesRead;

            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
            {
                memoryStream.Write(buffer, 0, bytesRead);
            }

            memoryStream.Position = 0;

            using (StreamReader reader = new StreamReader(memoryStream, Encoding.Unicode))
            {
                string convertedString = reader.ReadToEnd();
                Console.WriteLine("Converted String:");
                int a = 0;
                foreach (var item in convertedString.Split("--MIME_boundary"))
                {
                    using (StreamWriter writer = new StreamWriter($"received_data-{a}.jpeg"))
                    {
                        var enumerable = Regex.Split(item, "\r\n|\r|\n").Skip(4);
                        var data = string.Join(Environment.NewLine, enumerable.ToArray());

                        writer.Write(data.Trim(' '));
                    }

                    // using (FileStream fs = new FileStream($"data-{a}.bin", FileMode.Create))
                    // {
                    //     using (BinaryWriter bw = new BinaryWriter(fs, Encoding.Default))
                    //     {
                    //         bw.Write(item);
                    //     }
                    // }

                    a += 1;
                }
            }

            client.Close();
        }
    }
}